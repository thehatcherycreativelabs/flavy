<?php namespace Folour\Flavy\Extensions;

/**
 *
 * @author Vadim Bova <folour@gmail.com>
 * @link   http://github.com/folour | http://vk.com/folour
 *
 */

use Folour\Flavy\Exceptions\CmdException;
use Symfony\Component\Process\Process;

abstract class Commands
{
    /**
     *
     * Commands for get needed information from stdout
     *
     * @var array
     */
    
    protected $_cmd = [
        'get_formats' => [
            'regex'     => '/(?P<mux>(D\s|\sE|DE))\s(?P<format>\S{3,11})\s/',
            'cmd'       => '%s -formats',
            'returns'   => ['format', 'mux']
        ],

        'get_encoders' => [
            'regex'     => '/(?P<type>(A|V)).....\s(?P<format>\S{3,20})\s/',
            'cmd'       => '%s -encoders',
            'returns'   => ['type', 'format']
        ],

        'get_decoders' => [
            'regex'     => '/(?P<type>(A|V)).....\s(?P<format>\S{3,20})\s/',
            'cmd'       => '%s -decoders',
            'returns'   => ['type', 'format']
        ],

        'get_file_info' => [
            'cmd' => '%s -v quiet -print_format %s -show_format -show_streams -pretty -i "%s" 2>&1'
        ],

        'get_thumbnails' => [
            'cmd' => '%s -i "%s" -vf fps=1/%d -vframes %d "%s"'
        ],

        'concat_video' => [
            //'cmd' => '%s -auto_convert 1 -f concat -safe 0 -i %s -c:v libx264 -b 512k -flags +loop+mv4 -cmp 256 -partitions +parti4x4+parti8x8+partp4x4+partp8x8+partb8x8 -me_method hex -subq 7 -trellis 1 -refs 5 -bf 3 -flags2 +bpyramid+wpred+mixed_refs+dct8x8 -coder 1 -me_range 16 -g 250 -keyint_min 25 -sc_threshold 40 -i_qfactor 0.71 -qmin 10 -qmax 51 -qdiff 4 %s -y'

            'cmd' => '%s -auto_convert 1 -f concat -safe 0 -i %s -c:v libx264 -preset fast -b:v 500k -b:a 128k -c:a aac -qp 0 %s -y'
            
            /*
                 -profile:v baseline -level 3.0
                -c:v libx264 -preset ultrafast -qp 
            */
        ],


        'transition_crossfade' => [
            'cmd' => '%s -i %s -i %s -f lavfi -i color=black -filter_complex \
                    "[0:v]format=pix_fmts=yuva420p,fade=t=out:st=1:d=1:alpha=1,setpts=PTS-STARTPTS[va0];\
                    [1:v]format=pix_fmts=yuva420p,fade=t=in:st=0:d=1:alpha=1,setpts=PTS-STARTPTS+1/TB[va1];\
                    [2:v]scale=1920x1080,trim=duration=2[over];\
                    [over][va0]overlay[over1];\
                    [over1][va1]overlay=format=yuv420[outv]" \
                    -vcodec libx264 -map [outv] %s -y'
        ],

        'transition_slide_L_R' => [ // left to right slide-in
            'cmd' => '%s -i %s -i %s -filter_complex "overlay=\'if(gte(t,0), -w+t*960, NAN)\':(main_h-overlay_h)/2" -t 2 -vcodec libx264  -y %s'
            //'cmd' => '%s -i %s -i %s -filter_complex "[0:v][1:v]overlay=x=\'if(lte(-w+(t)*500,w/2),-w+(t)*500,w/2)\':y=0[out]" -vcodec libx264 -map \'[out]\' -y %s'
            // -pixel_format yuv420p
        ],
        'transition_slide_R_L' => [ // right to left slide-in
            'cmd' => '%s -i %s -i %s -filter_complex "overlay=\'if(gte(t,0), +w-t*960, NAN)\':(main_h-overlay_h)/2" -t 2 -vcodec libx264  -y %s'
        ],
        'transition_slide_T_B' => [ // right to left slide-in
            'cmd' => '%s -i %s -i %s -filter_complex "overlay=(main_h-overlay_h)/2:\'if(gte(t,0), -h+t*550, NAN)\'" -t 2 -vcodec libx264  -y %s'
        ],
        'transition_slide_B_T' => [ // right to left slide-in
            'cmd' => '%s -i %s -i %s -filter_complex "overlay=(main_h-overlay_h)/2:\'if(gte(t,0), +h-t*550, NAN)\'" -t 2 -vcodec libx264  -y %s'
        ],



        'create_slide' => [
            'cmd' => '%s -i %s -i %s -filter_complex "scale=1920x1080,overlay=x=(main_w-overlay_w)/2:y=(main_h-overlay_h)/2" -c:v libx264 -t %d -b:v %d -an -r 24 -preset ultrafast -qp 0 %s -y'
        ],

        'create_slide_from_img' => [
            'cmd' => '%s -loop 1 -i %s -i %s -filter_complex "scale=1920x1080,overlay=x=(main_w-overlay_w)/2:y=(main_h-overlay_h)/2" -c:v libx264 -t %d -b:v %d -an -r 24 -preset ultrafast -qp 0 %s -y'
        ],

        'add_soundtrack' => [
            'cmd' => '%s -i %s -i %s -acodec libmp3lame -vcodec copy -t %s %s'
        ],

        // 'concat_video_glue' => [
        //     // 'cmd' => '%s -i "concat:%s" -c copy %s'
        //        'cmd' => '%s -i %s -c copy -bsf:v h264_mp4toannexb -f mpegts %s'
        // ]

    ];

    /**
     *
     * Run command and return output
     *
     * @param string $cmd shell command or key of pre-defined commands
     * @param array $args Command arguments
     *
     * @return string|bool Command output
     * @throws CmdException
     */
    protected function runCmd($cmd, $args = [])
    {
        $prop = isset($this->_cmd[$cmd]) ? $this->_cmd[$cmd] : [];
        $cmd = isset($this->_cmd[$cmd]) ? $this->_cmd[$cmd]['cmd'] : $cmd;

        $process = new Process(vsprintf($cmd, array_values($args)));
        $process->run();

        if($process->isSuccessful()) {
            $output = $process->getOutput();

            if(isset($prop['regex'])) {
                if(preg_match_all($prop['regex'], $output, $matches)) {
                    return array_only($matches, $prop['returns']);
                }
            }

            return $output;
        }

        throw new CmdException($process->getErrorOutput());
    }
}