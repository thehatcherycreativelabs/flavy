<?php namespace Folour\Flavy;

/**
 *
 * FFmpeg layer for Laravel 5.2
 * This is a fully reworked fork of http://github.com/rafasamp/sonus package.
 *
 * @author Vadim Bova <folour@gmail.com>
 * @link   http://github.com/folour | http://vk.com/folour
 *
 */

use \File;
use \Folour\Flavy\Extensions\Base;
use \Folour\Flavy\Exceptions\NotWritableException;
use \Folour\Flavy\Exceptions\FileNotFoundException;

class Flavy extends Base
{
    /**
     * Flavy version
     */
    const VERSION = '1.0';

    /**
     *
     * Array of command parameters from builder
     *
     * @var array
     */
    protected $parameters = [];

    /**
     *
     * Output file path
     *
     * @var string
     */
    protected $outputPath;

    /**
     *
     * Log path
     *
     * @var string
     */
    protected $logPath;

    /**
     *
     * Array of possible methods for execute
     *
     * @var array
     */
    private $nextContext = 'input';

    /**
     *
     * Flavy constructor.
     *
     * @param array $config
     */
    public function __construct(array $config)
    {
        parent::__construct($config);
    }

    /**
     *
     * Returns array (for json format) or string (for other formats) with file information
     *
     * @param string $file File path
     * @param string $format output format, supported json, xml and csv
     * @param bool   $decode decode json to array
     *
     *
     * @return array|string
     * @throws FileNotFoundException
     */
    public function info($file, $format = 'json', $decode = true)
    {
        $this->isPossible('input');
        if(!File::exists($file)) {
            throw new FileNotFoundException('The input file "'.$file.'" not exists!');
        }

        $format = in_array($format, ['json', 'xml', 'csv']) ? $format : 'json';
        $data = $this->runCmd('get_file_info', [$this->config['ffprobe'], $format, $file]);
        if($format == 'json' && $decode === true) {
            return json_decode($data, true);
        }

        return $data;

    }

    /**
     *
     * Create thumbnails from video
     *
     * @param string $file input file
     * @param string $output_path output file path template
     * @param int $count thumbnails count
     * @param int|null $interval frames interval in seconds
     *
     * @return bool|string
     * @throws FileNotFoundException
     * @throws NotWritableException
     */
    public function thumbnail($file, $output_path, $count = 1, $interval = null)
    {
        $this->isPossible('input');
        if(!File::exists($file)) {
            throw new FileNotFoundException('The input file "'.$file.'" not exists!');
        }
        if(!File::isWritable(dirname($output_path))) {
            throw new NotWritableException('The output path "'.$output_path.'" is not writable!');
        }

        if($interval == null) {
            $interval = 10;
            $info = $this->info($file);
            if(is_array($info)) {
                $duration = $this->timestamp(explode('.', $info['format']['duration'])[0]);

                $interval = round($duration / $count);
            }
        }

        return $this->runCmd('get_thumbnails', [
            $this->config['ffmpeg'], $file, $interval, $count, $output_path
        ]);
    }

    public function concat($output, $slideshow_file)
    {
        // ($slide_paths, $thepath, $processing_folder, $data_file_path)
        return $this->runCmd('concat_video', [
            $this->config['ffmpeg'], stripslashes($slideshow_file), stripslashes($output)
        ]);
    }

    public function add_overlay($from_path,$overlay_path,$out_path_with_overlay)
    {
        // ($slide_paths, $thepath, $processing_folder, $data_file_path)
        return $this->runCmd('add_overlay', [
            $this->config['ffmpeg'], stripslashes($from_path), stripslashes($overlay_path), stripslashes($out_path_with_overlay)
        ]);
    }

    public function add_soundtrack($audio_path,$video_path,$duration,$out_path_with_audio)
    {
        // ($slide_paths, $thepath, $processing_folder, $data_file_path)
        return $this->runCmd('add_soundtrack', [
            $this->config['ffmpeg'], stripslashes($audio_path), stripslashes($video_path), stripslashes($duration), stripslashes($out_path_with_audio)
        ]);
    }

    public function create_slide($from_path,$overlay_path,$out_path,$duration,$bitrate)
    {
        return $this->runCmd('create_slide', [
            $this->config['ffmpeg'], stripslashes($from_path), stripslashes($overlay_path), stripslashes($duration), stripslashes($bitrate), stripslashes($out_path)
        ]);
    }

    public function create_slide_from_img($from_path,$overlay_path,$out_path,$duration,$bitrate)
    {
        return $this->runCmd('create_slide_from_img', [
            $this->config['ffmpeg'], stripslashes($from_path), stripslashes($overlay_path), stripslashes($duration), stripslashes($bitrate), stripslashes($out_path)
        ]);
    }


    /**
     *
     * Transition Filters
     */
    public function transition_crossfade($slide_path_1,$slide_path_2,$output_path)
    {
        // ($slide_paths, $thepath, $processing_folder, $data_file_path)
        return $this->runCmd('transition_crossfade', [
            $this->config['ffmpeg'], stripslashes($slide_path_1), stripslashes($slide_path_2), stripslashes($output_path)
        ]);
    }
    public function transition_slide_L_R($slide_path_1,$slide_path_2,$output_path)
    {
        // ($slide_paths, $thepath, $processing_folder, $data_file_path)
        return $this->runCmd('transition_slide_L_R', [
            $this->config['ffmpeg'], stripslashes($slide_path_1), stripslashes($slide_path_2), stripslashes($output_path)
        ]);
    }
    public function transition_slide_R_L($slide_path_1,$slide_path_2,$output_path)
    {
        // ($slide_paths, $thepath, $processing_folder, $data_file_path)
        return $this->runCmd('transition_slide_R_L', [
            $this->config['ffmpeg'], stripslashes($slide_path_1), stripslashes($slide_path_2), stripslashes($output_path)
        ]);
    }
    public function transition_slide_T_B($slide_path_1,$slide_path_2,$output_path)
    {
        // ($slide_paths, $thepath, $processing_folder, $data_file_path)
        return $this->runCmd('transition_slide_T_B', [
            $this->config['ffmpeg'], stripslashes($slide_path_1), stripslashes($slide_path_2), stripslashes($output_path)
        ]);
    }
    public function transition_slide_B_T($slide_path_1,$slide_path_2,$output_path)
    {
        // ($slide_paths, $thepath, $processing_folder, $data_file_path)
        return $this->runCmd('transition_slide_B_T', [
            $this->config['ffmpeg'], stripslashes($slide_path_1), stripslashes($slide_path_2), stripslashes($output_path)
        ]);
    }

    /**
     *
     * Set input file
     *
     * @param $file
     * @return $this
     * @throws FileNotFoundException
     */
    public function from($file)
    {
        $this->isPossible('input');
        if(!File::exists($file)) {
            throw new FileNotFoundException('The input file "'.$file.'" not exists!');
        }

        $this->parameters[] = sprintf('%s -i "%s"', $this->config['ffmpeg'], $file);
        $this->nextContext = 'output';

        return $this;
    }

    /**
     *
     * Set input image for image->video translation
     *
     * @param $file
     * @return $this
     * @throws FileNotFoundException
     */
    public function fromImg($file)
    {
        $this->isPossible('input');
        if(!File::exists($file)) {
            throw new FileNotFoundException('The input file "'.$file.'" not exists!');
        }

        $this->parameters[] = sprintf('%s -loop 1 -i "%s"', $this->config['ffmpeg'], $file);
        $this->nextContext = 'output';

        return $this;
    }

    /**
     *
     * Set output file
     *
     * @param string $outputPath
     *
     * @return $this
     * @throws NotWritableException
     */
    public function to($outputPath)
    {
        $this->isPossible('output');
        if(!File::isWritable(dirname($outputPath))) {
            throw new NotWritableException('The output path "'.$outputPath.'" is not writable!');
        }

        $this->outputPath = $outputPath;
        $this->nextContext = 'run_or_param';

        return $this;
    }

    /**
     *
     * Set audio codec
     *
     * @param string $codec
     * @return $this
     */
    public function aCodec($codec)
    {
        $this->isPossible('run_or_param');
        $this->parameters[] = sprintf('-acodec %s', $codec);

        return $this;
    }

    /**
     *
     * Set video codec
     *
     * @param string $codec
     * @return $this
     */
    public function vCodec($codec)
    {
        $this->isPossible('run_or_param');

        $this->parameters[] = sprintf('-vcodec %s', $codec);

        return $this;
    }

    /**
     *
     * Set audio bitrate
     *
     * @param int $bitrate
     * @return $this
     */
    public function aBitrate($bitrate)
    {
        $this->isPossible('run_or_param');
        $this->parameters[] = sprintf('-b:a %dk', $bitrate);

        return $this;
    }

    /**
     *
     * Set video bitrate
     *
     * @param int $bitrate
     * @return $this
     */
    public function vBitrate($bitrate)
    {
        $this->isPossible('run_or_param');
        $this->parameters[] = sprintf('-b:v %dk', $bitrate);

        return $this;
    }

    /**
     *
     * Set audio channels
     *
     * @param int $channels
     * @return $this
     */
    public function channels($channels)
    {
        $this->isPossible('run_or_param');
        $this->parameters[] = sprintf('-ac %d', $channels);

        return $this;
    }

    /**
     *
     * Set audio sample rate
     *
     * @param int $rate
     * @return $this
     */
    public function sampleRate($rate)
    {
        $this->isPossible('run_or_param');
        $this->parameters[] = sprintf('-ar %d', $rate);

        return $this;
    }

    /**
     *
     * Set video frame rate
     *
     * @param int $rate
     * @return $this
     */
    public function frameRate($rate)
    {
        $this->isPossible('run_or_param');
        $this->parameters[] = sprintf('-r %d', $rate);

        return $this;
    }

    public function pxFmt($pxfmt)
    {
        $this->isPossible('run_or_param');
        $this->parameters[] = sprintf('-pix_fmt %d', $pxfmt);

        return $this;
    }
    public function duration($duration)
    {
        $this->isPossible('run_or_param');
        $this->parameters[] = sprintf('-t %d', $duration);

        return $this;
    }
    public function scale($scale)
    {
        $this->isPossible('run_or_param');
        $this->parameters[] = sprintf('-vf scale=%s', $scale);

        return $this;
    }
    public function vf($vf)
    {
        $this->isPossible('run_or_param');
        $this->parameters[] = sprintf('-vf "%s"', $vf);

        return $this;
    }
    public function ss($ss)
    {
        $this->isPossible('run_or_param');
        $this->parameters[] = sprintf('-ss "%s"', $ss);

        return $this;
    }
    public function addOverlay($addOverlay)
    {
        $this->isPossible('run_or_param');
        $this->parameters[] = sprintf('-i $s -filter_complex "overlay=0:0"', $addOverlay);
        // ffmpeg -i $out_path -i $overlay_path -filter_complex "overlay=0:0" $filename.'_out.mpeg'

        return $this;
    }
    public function overwrite()
    {
        $this->isPossible('run_or_param');
        $this->parameters[] = '-y';

        return $this;
    }
    public function an()
    {
        $this->isPossible('run_or_param');
        $this->parameters[] = '-an';

        return $this;
    }

    public function logTo($logPath)
    {
        $this->isPossible('run_or_param');
        $this->logPath = $logPath;

        return $this;
    }

    /**
     *
     * Run a conversion process
     *
     * @throws Exceptions\CmdException
     */
    public function run()
    {
        $this->parameters[] = sprintf('"%s"', $this->outputPath);
        if($this->logPath !== null) {
            $this->parameters[] = sprintf('>"%s" 2>&1', $this->logPath);
        }

        $command = implode(' ', $this->parameters);

        $this->parameters = [];
        $this->nextContext = 'input';
        $this->logPath = $this->outputPath = null;

        $this->runCmd($command);
    }

    /**
     * @param string $context context name
     *
     * @return bool
     * @throws \BadMethodCallException
     */
    private function isPossible($context)
    {
        if($this->nextContext != $context) {
            throw new \BadMethodCallException('This method is not possible on this context');
        }

        return true;
    }
}
